//
//  UIButtonExtensions.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import Foundation
import UIKit


extension UIButton {
    
    
    func defaultButton(){
        
        layer.backgroundColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00).cgColor
        layer.cornerRadius = 10
        
    }
    
    func homeButton(){
        
        layer.cornerRadius = 15
    }
    
    
}
