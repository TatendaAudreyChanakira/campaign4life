//
//  OnBoardingModel.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import Foundation
import UIKit

class OnBoardingModel {
    
    var title: String
    var subTitle: String
    var descriptionText: String
    var img: UIImage
    
    init(title: String,subTitle: String , descriptionText: String, imgName: String) {
        self.title = title
        self.subTitle = subTitle
        self.descriptionText = descriptionText
        self.img = UIImage(named: imgName)!
    }
}
