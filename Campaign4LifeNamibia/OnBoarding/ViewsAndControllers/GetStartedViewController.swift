//
//  GetStartedViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import UIKit

class GetStartedViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var getStartedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
    }


    
    func setupUI(){
        
        getStartedButton.defaultButton()
        
    }
    
    @IBAction func getStartedClick(_ sender: UIButton) {
        
        performSegue(withIdentifier: "gotoOB", sender: nil)
    }
    
}
