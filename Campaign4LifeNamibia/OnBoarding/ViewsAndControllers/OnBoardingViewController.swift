//
//  OnBoardingViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import UIKit


class OnBoardingViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var headerTextLabel: UILabel!
    
    var window: UIWindow?
    
    // MARK: - Constants
    let reuseIdentifier = "OnboardingCollectionViewCell"
    let onBoardingText = ["Fight COVID & Get Vaccinated", "Demystify the Myths", "And have a healthy Namibia!"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    
    
    func setupUI(){
        
        pageControl.numberOfPages = onBoardingText.count
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        continueButton.defaultButton()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: reuseIdentifier, bundle: .main), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    
    @IBAction func continueButtonClick(_ sender: UIButton) {
        
        //Apparently there is a bug in iOS 14, hence the need to to disable and enable paging.
        collectionView.isPagingEnabled = false
        
        let indexPath = IndexPath(item: pageControl.currentPage + 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
        pageControl.currentPage += 1
        
        collectionView.isPagingEnabled = true
        
        if pageControl.currentPage == onBoardingText.count - 1  {
            print("Last Page")
           
          performSegue(withIdentifier: "gotoMain", sender: nil)
            
        }
        
  
        
    }
    
}

extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return onBoardingText.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! OnboardingCollectionViewCell
        
//        cell.mythorFactLabel.text = onBoardingText[indexPath.row].title
//        cell.topicLabel.text = onBoardingText[indexPath.row].subTitle
//        cell.descriptionLabel.text = onBoardingText[indexPath.row].descriptionText
        
        headerTextLabel.text = onBoardingText[indexPath.row]
        
        
        
        return cell
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let bounds = self.collectionView.bounds
        let midpoint =  CGPoint(x: bounds.midX, y: bounds.midY)
        if let indexPath = self.collectionView.indexPathForItem(at: midpoint) {
            pageControl.currentPage = indexPath.item
            
        }
    }
    
}


extension OnBoardingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width : CGFloat?
        var height : CGFloat?
        
        height = collectionView.frame.height
        width = collectionView.frame.width
        
        return CGSize(width: width!, height: height!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    }
    
    
    
    
}

