//
//  HomeAdCollectionViewCell.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit

class HomeAdCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var homeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
