//
//  VaccineDetailViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit
import WebKit

class VaccineDetailViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var webView: WKWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Sinopharm"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let url = URL(string: "https://www.who.int/news-room/feature-stories/detail/the-sinopharm-covid-19-vaccine-what-you-need-to-know")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }


}
