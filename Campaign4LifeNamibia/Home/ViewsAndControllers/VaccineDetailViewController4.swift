//
//  VaccineDetailViewController4.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit
import WebKit

class VaccineDetailViewController4: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Johnson & Johnson"
        navigationController?.navigationBar.barTintColor = UIColor(red: 1.00, green: 0.18, blue: 0.33, alpha: 1.00)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let url = URL(string: "https://www.jnj.com/johnson-johnson-covid-19-vaccine-authorized-by-u-s-fda-for-emergency-usefirst-single-shot-vaccine-in-fight-against-global-pandemic")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }


}
