//
//  VaccineDetailViewController2.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit
import WebKit
class VaccineDetailViewController2: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "AstraZeneca"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.98, green: 0.64, blue: 0.11, alpha: 1.00)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let url = URL(string: "https://www.who.int/news-room/feature-stories/detail/the-oxford-astrazeneca-covid-19-vaccine-what-you-need-to-know")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }

  

}
