//
//  HomeViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sinopharmButton: UIButton!
    @IBOutlet weak var astraButton: UIButton!
    @IBOutlet weak var sinovacButton: UIButton!
    @IBOutlet weak var jjButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    // MARK: - Constants
    let reuseIdentifier = "HomeAdCollectionViewCell"
    let homeImages = [HomeImageModel(imgName: "ad3"), HomeImageModel(imgName: "ad2"), HomeImageModel(imgName: "ad1")]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Home"
        
        setupUI()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        registerNIB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00)
        
        pageControl.numberOfPages = homeImages.count
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
    }
    
    
    func setupUI(){
        
        sinopharmButton.homeButton()
        astraButton.homeButton()
        sinovacButton.homeButton()
        jjButton.homeButton()
        
    }
    
    func registerNIB(){
        
        collectionView.register(UINib(nibName: reuseIdentifier, bundle: .main), forCellWithReuseIdentifier: reuseIdentifier)
        
    }
    

    
    
    // MARK: - Button Actions
    
    @IBAction func sinopharmClick(_ sender: UIButton) {
        
        performSegue(withIdentifier: "gotoDetails1", sender: nil)
    }
    
    @IBAction func astraZenecaClick(_ sender: UIButton) {
     
        performSegue(withIdentifier: "gotoDetails2", sender: nil)
    }
    
    @IBAction func sinovacClick(_ sender: UIButton) {
        
        performSegue(withIdentifier: "gotoDetails3", sender: nil)
    }
    
    @IBAction func jjClick(_ sender: UIButton) {
       
        performSegue(withIdentifier: "gotoDetails4", sender: nil)
    }
    
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        homeImages.count
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeAdCollectionViewCell
        
        cell.homeImageView.image = homeImages[indexPath.row].img
        
        return cell
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let bounds = self.collectionView.bounds
        
        let midpoint =  CGPoint(x: bounds.midX, y: bounds.midY)
        
        if let indexPath = self.collectionView.indexPathForItem(at: midpoint) {
            pageControl.currentPage = indexPath.item
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width : CGFloat?
        var height : CGFloat?
        
        height = collectionView.frame.size.height
        width = (view.bounds.width)
        
        return CGSize(width: width!, height: height!)
    }
}

