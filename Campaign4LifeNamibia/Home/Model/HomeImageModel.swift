//
//  HomeImageModel.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import Foundation
import UIKit

class HomeImageModel {
    
    var img: UIImage
    
    init( imgName: String) {
        
        self.img = UIImage(named: imgName)!
    }
}
