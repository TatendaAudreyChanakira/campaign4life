//
//  ProfileViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var checkMarkImageView: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var level1: UIButton!
    @IBOutlet weak var level2: UIButton!
    @IBOutlet weak var level3: UIButton!
    @IBOutlet weak var level4: UIButton!
    @IBOutlet weak var star: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        title = "Profile"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00)
        
        setupUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        progressView.progress = 1.0
        progressView.backgroundColor = UIColor(red: 0.20, green: 0.78, blue: 0.35, alpha: 1.00)
        containerView.backgroundColor = UIColor(red: 0.20, green: 0.78, blue: 0.35, alpha: 1.00)
        star.isHidden = false
        level1.backgroundColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00)
        level1.setTitleColor(UIColor.white, for: .normal)
    }
    
    func setupUI() {
        
        level1.homeButton()
        level2.homeButton()
        level3.homeButton()
        level4.homeButton()
        
        progressView.layer.cornerRadius = 20
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 20
        progressView.subviews[1].clipsToBounds = true
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(ovalIn: self.containerView.bounds).cgPath;
        self.containerView.layer.mask = maskLayer
        
        let radius = imageView.frame.width/2.0
        imageView.layer.cornerRadius = radius
        imageView.layer.masksToBounds = true
        
        containerView.backgroundColor = UIColor.red
        
    }
    
    @IBAction func vaccineClick(_ sender: UIButton) {
        
        performSegue(withIdentifier: "gotoVaccine", sender: nil)
        
    }
    
    @IBAction func level2Click(_ sender: UIButton) {
        
        self.showAlert(title: "Share", message: "Share a selfie on our Official FaceBook page using the #Campaign4LifeNamibia. A code will be sent to you to unlock the next level.", preferredStyle: .alert, actionTitle: "Okay")
    }
    
}
