//
//  VaccineViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit
import Photos

class VaccineViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var firstDoseImageView: UIImageView!
    @IBOutlet weak var secondDoseImageView: UIImageView!
    
    
    
    // MARK: - Variables
    var imagePicker = UIImagePickerController()
    ///define the camera picker
    let cameraPicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Vaccination"
        
        self.imagePicker.delegate = self

        
        submitButton.isEnabled = false
        submitButton.layer.cornerRadius = 10
       
    }
    
    
    
    @IBAction func submitClick(_ sender: UIButton) {
        
        self.showAlertWithIcon(title: "Submitted", message: "You have successfully submitted your Vaccination information. Check your Profile progress for an update on your vaccination status", imageName: "check-icon", preferredStyle: .alert, actionTitle: "Okay") { _ in
            
            
            self.firstDoseImageView.tintColor = UIColor(red: 0.20, green: 0.78, blue: 0.35, alpha: 1.00)
        }
    }
    
    
  


    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        showAlertWith3Actions()
    }
    
    func showAlertWith3Actions() {
        
        let alert = UIAlertController(title: "Add image…", message: "Please select a source for the image you wish to upload", preferredStyle: UIAlertController.Style.alert)
        for i in ["Camera", "Gallery","Remove Current","Cancel"] {
            alert.addAction(UIAlertAction(title: i, style: UIAlertAction.Style.default, handler: alertActions)
            )}
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertActions(action: UIAlertAction) {
        if action.title == "Camera"{
            ///define the image picker
            
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                present(imagePicker,animated: true,completion: nil)
            }else{
                self.showAlert(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert, actionTitle: "OK")
            }
            
        }else if action.title == "Gallery"{
            selectPhotoFromGallery()
            submitButton.defaultButton()
        }else if action.title == "Remove Current" {
            
            cardImageView.image = nil
            cardImageView.image = UIImage.init(named: "upload")
            cardImageView.contentMode = .scaleAspectFit
            
            submitButton.isEnabled = false
            submitButton.layer.cornerRadius = 10
            submitButton.backgroundColor = UIColor.gray
            
            do {
               // profile?.photo = nil
                
              // try Context.save()
            } catch {
                print("Error removing photo from profile: ", error.localizedDescription)
            }
        } else {
            
        }
    }
    
    ///Method enables user to select image from photo gallery
    func selectPhotoFromGallery() {
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.notDetermined || PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            //TYPE = "GALLERY"
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(imagePicker, animated: true, completion: nil)
            }
            else {
                imagePicker.modalPresentationStyle = .popover
                present(imagePicker, animated: true, completion: nil)//4
                imagePicker.popoverPresentationController?.sourceView = self.navigationItem.rightBarButtonItem?.customView
                imagePicker.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                
                imagePicker.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.left
                imagePicker.popoverPresentationController?.sourceRect = CGRect(x: 80, y: 10, width: 0, height: 0)
                
            }
        }
        else if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.denied
        {
            turnOnGallery()
        }
    }
    
    ///Method makes request to access device gallery
    func turnOnGallery(){
        let alertVC = UIAlertController(
            title: "Photo Library Unavailable",
            message: "Please check device settings to allow photo library access",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: { (Bool) in
                })
            }
        }
        alertVC.addAction(okAction)
        alertVC.addAction(settingsAction)
        present(alertVC,
                animated: true,
                completion: nil)
    }
    
    

}

extension VaccineViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        cardImageView.image = image
        submitButton.isEnabled = true
        submitButton.defaultButton()
        
        //TODO: Save Image to CoreData
//        if let imageData = image.pngData() {
//            profile?.photo = imageData.base64EncodedString()
//        }
//
//        do {
//            try Context.save()
//        } catch {
//            print("Error saving profile photo: ", error.localizedDescription)
//        }
        
        dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
