//
//  NotificationViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 31/07/2021.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let reuseIdentifier = "NotificationTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Notifications"
        
        setupUI()
        registerNIB()
    }


    func setupUI(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func registerNIB(){
        tableView.register(UINib(nibName: reuseIdentifier, bundle: .main), forCellReuseIdentifier: reuseIdentifier)
    }

}


extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! NotificationTableViewCell
        
        if indexPath.row == 0 {
            
            cell.containerView.backgroundColor = UIColor.orange
            cell.title.text = "2nd COVID-19 Jab"
            cell.details.text = "Kindly note that your second dose of your vaccine is tomorrow. Bring your ID and Vaccination card along."
            
        } else {
        
            cell.containerView.backgroundColor = UIColor.systemBlue
            cell.title.text = "New Vaccination Center"
            cell.details.text = "A new vaccination center has been opened in Katutura."
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
