//
//  InfoViewController.swift
//  Campaign4LifeNamibia
//
//  Created by Audrey Chanakira on 30/07/2021.
//

import UIKit
import SwiftyGif

class InfoViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var gifView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var learnMoreButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Info"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.07, green: 0.38, blue: 0.53, alpha: 1.00)
        
        setupUI()
    }

    
   func setupUI(){
    
    containerView.layer.cornerRadius = 15
    innerView.layer.cornerRadius = 15
    learnMoreButton.layer.cornerRadius = 15
    
    imageView.layer.cornerRadius = 15
    imageView.layer.masksToBounds = true
    
    // Do any additional setup after loading the view.
    do {
        let gif = try UIImage(gifName: "nam-gif.gif")
        self.gifView.setGifImage(gif)
        self.gifView.loopCount = -1
    } catch {
        print(error)
    }
    }

    
    @IBAction func learnMoreClick(_ sender: UIButton) {
        
        self.showAlertWithAction(title: "New Vaccination Center", message: "Great news! A new vaccination booth will be available ar Soweto Market. The booth will be open on friday 6 August 2021. The Sinopharm Vaccine will be available.", preferredStyle: .alert, actionTitle: "Okay") { _ in
            
        }
    }
    
}
